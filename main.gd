extends Control

var upnp
var state

func _on_Port_text_changed(new_text):
	if _is_number(new_text) and new_text != "" and int(new_text) > 0 and int(new_text) < 65536:
		$Start.disabled = false
	else:
		$Start.disabled = true
		$Port.text = ""
		
func _on_Start_button_down():
	_toggle_forward_port()

func _ready():
	_init_validator()
	
	upnp = UPNP.new()
	_update_status(upnp.discover(2000, 2, "InternetGatewayDevice"))

	state = false
	
	OS.set_window_position((OS.get_screen_size() - OS.get_window_size()) / 2)

func _exit_tree():
	if state:
		_toggle_forward_port()

func _toggle_forward_port():
	if state:
		state = false
		
		$Port.editable = true
		$Start.text = "Start"
		
		_update_status(upnp.delete_port_mapping (int($Port.text), "UDP"))
		_update_status(upnp.delete_port_mapping (int($Port.text), "TCP"))
		
	else:
		state = true
		
		$Port.editable = false
		$Start.text = "Stop"
		
		_update_status(upnp.add_port_mapping(int($Port.text), 0, "", "UDP", 0))
		_update_status(upnp.add_port_mapping(int($Port.text), 0, "", "TCP", 0))

func _update_status(status):
	match status:
		UPNP.UPNP_RESULT_SUCCESS:
			if state == null:
				$Status.text = "READY"
			elif state == true:
				$Status.text = "FORWARDING PORT: " + $Port.text
			else:
				$Status.text = "STOPPED"
				
		UPNP.UPNP_RESULT_NOT_AUTHORIZED:
			$Status.text = "REQUEST REJECTED"
			state = false
			$Port.editable = true
			$Start.text = "Start"
			
		_:
			$Port.editable = false
			$Start.disabled = true
			$Status.text = "UPNP NOT SUPPORTED"
		
var ip_regex
var ipv6_regex
var number_regex

func _init_validator():
	number_regex = RegEx.new()
	number_regex.compile("^[0-9]*$")

func _is_number(input: String):
	if number_regex.search(input):
		return true
	else:
		return false

